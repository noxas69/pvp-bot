import discord
from discord.utils import get
from discord.ext import commands
import wow

bot = commands.Bot(command_prefix="!")
TOKEN = "Nzk4OTM3NzIwOTQzOTM1NTI4.X_8SvA.LFF078ahLEkHkpvvMx50GDgSp-U"

#@bot.event
#async def on_message(message):
#    if message.content.startswith("!kek"):
#        channel = bot.get_channel(798943575474962432)
#        await channel.send('hello')
        

@bot.command()
async def pvp(ctx, char, serv=""):
    wow_token = wow.get_token()

    # On vérifie si il y a un - entre le nom du personnage et le serveur, si oui on split la chaine pour créer 2 arguments.
    if "-" in char:
        x = char.split("-")
        char = x[0]
        serv = x[1]

    # On s'assure d'enlever les majuscules dans l'input
    character = char.lower()
    server = serv.lower()

    if "twisting" in serv:
        server = "twisting-nether"

    if "tarren" in serv:
        server = "tarren-mill"

    if "argent" in serv:
        server = "argent-dawn"

    server = server.replace("'", "")



    try:
        r = wow.get_pvp_arena(character, server, wow_token)
        nb = wow.get_stats(character, server, wow_token)
        xp = wow.get_xp(character, server, wow_token)
        xpv2 = xp[0]
        xpv3 = xp[1]
        xprbg = xp[2]

        if xpv2 == 2200:
            xpv2 = "2200++"

        if xpv3 == 3000:
            xpv3 = "Gladiateur"

        if xprbg == 3000:
            xprbg = "Héro de la horde / alliance"

        if xpv2 == 0:
            xpv2 = "- de 2000"

        if xprbg ==0:
            xprbg = "- de 2000"

        await ctx.send(f"""
        Statistiques pvp :
        ilvl: {r[0]}
        Nombre de matchs d'arènes depuis création : {nb}
        ====== 2v2 ======
        Côte actuelle : {r[1]}
        Victoires : {r[2]}
        Défaites : {r[3]}
        XP max : {xpv2} \n
        ====== 3v3 ======
        Côte actuelle : {r[4]}
        Victoires : {r[5]}
        Défaites : {r[6]}
        XP max : {xpv3} \n
        ====== RBG ======
        Côte actuelle : {r[7]}
        Victoires : {r[8]}
        Défaites : {r[9]}
        XP max : {xprbg}
        """)


    except AttributeError:
        await ctx.send("Impossible de trouver le personnage")



        
bot.run(TOKEN)
