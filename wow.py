import requests
import json



def get_token():
    ID = ""
    SECRET = ""
    data = {"grant_type": "client_credentials"}
    response = requests.post("https://eu.battle.net/oauth/token", data=data, auth=(ID, SECRET))
    TOKEN = response.json().get("access_token")
    header = {"Authorization" : "Bearer " + TOKEN}
    return header



def get_realm():
    """
    fournit la liste des royaumes de world of warcraft
    """
    servers = []
    data = {"region" : "eu", "namespace" : "dynamic-eu", "locale" : "fr_FR"}
    r = requests.get(URL + "/data/wow/realm/index", headers=get_token(), params=data)
    names = r.json().get("realms")
    for n in names:
        servers.append(n.get("name"))
    servers.sort()
    return servers



def get_ilvl(char_name, server_name, token):
    
    URL = "https://eu.api.blizzard.com"
    data = {"region" : "eu", "namespace" : "profile-eu", "locale" : "fr_FR"}
    r = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}", headers=token, params=data)
    
    if r.json().get("code") == 404:
        URL = "https://us.api.blizzard.com"
        data = {"region" : "us", "namespace" : "profile-us", "locale" : "fr_FR"}
        r = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}", headers=token, params=data)

    ilvl = r.json().get("equipped_item_level")
    return ilvl



def get_pvp_arena(char_name, server_name, token):


    URL = "https://eu.api.blizzard.com"
    data = {"region" : "eu", "namespace" : "profile-eu", "locale" : "fr_FR"}
    v2 = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/pvp-bracket/2v2", headers=token, params=data)
    v3 = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/pvp-bracket/3v3", headers=token, params=data)
    rbg = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/pvp-bracket/rbg", headers=token, params=data)

    if v2.json().get("code") == 404:
        URL = "https://us.api.blizzard.com"
        data = {"region" : "us", "namespace" : "profile-us", "locale" : "fr_FR"}
        v2 = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/pvp-bracket/2v2", headers=token, params=data)
        v3 = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/pvp-bracket/3v3", headers=token, params=data)
        rbg = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/pvp-bracket/rbg", headers=token, params=data)

    
    statsv2 = v2.json().get("season_match_statistics")
    statsv3 = v3.json().get("season_match_statistics")
    statsrbg = rbg.json().get("season_match_statistics")

    crv2 = v2.json().get("rating")
    winv2 = statsv2.get("won")
    losev2 = statsv2.get("lost")
    crv3 = v3.json().get("rating")
    winv3 = statsv3.get("won")
    losev3 = statsv3.get("lost")
    crv3 = v3.json().get("rating")
    crrbg = rbg.json().get("rating")
    winrbg = statsrbg.get("won")
    loserbg = statsrbg.get("lost")
    ilvl = get_ilvl(char_name, server_name, token)

    return(ilvl,crv2,winv2,losev2,crv3,winv3,losev3,crrbg,winrbg,loserbg)


    
def get_stats(char_name, server_name, token):
    URL = "https://eu.api.blizzard.com"
    data = {"region" : "eu", "namespace" : "profile-eu", "locale" : "fr_FR"}
    r = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/achievements/statistics", headers=token, params=data)

    if r.json().get("code") == 404:
        URL = "https://us.api.blizzard.com"
        data = {"region" : "us", "namespace" : "profile-us", "locale" : "fr_FR"}
        r = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/achievements/statistics", headers=token, params=data)


    for c in r.json().get("categories"):
        if c.get("sub_categories"):
            for x in c.get("sub_categories"):
                if x.get("statistics"):
                    for s in x.get("statistics"):
                        if s.get("name") == "Nombre de matchs d'arènes":
                            nb = str(s.get("quantity"))
                            nb = nb.split(".")
                                         
    
    return(nb[0])
                    


def get_xp(char_name, server_name, token):

    URL = "https://eu.api.blizzard.com"
    data = {"region" : "eu", "namespace" : "profile-eu", "locale" : "fr_FR"}
        # XP EN PVP VIA LES HF
    xp = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/achievements", headers=token, params=data)
    
    if xp.json().get("code") == 404:
        URL = "https://us.api.blizzard.com"
        data = {"region" : "us", "namespace" : "profile-us", "locale" : "fr_FR"}
        xp = requests.get(URL + f"/profile/wow/character/{server_name}/{char_name}/achievements", headers=token, params=data)

    xpv2 = []
    xpv3 = []
    xprbg = []

    ## xp en v2
    for achiv in xp.json().get("achievements"):
        a = achiv.get("achievement")["name"]
        #### xp en V2 #####
        ####

        if "Just the Two of Us: 2400" in a:
            xpp = achiv.get("criteria")["is_completed"]
            if xpp is True:
                xpv2.append(2400)
                
                
        elif "Just the Two of Us: 2200" in a:
            xpp = achiv.get("criteria")["is_completed"]
            if xpp is True:
                xpv2.append(2200)
                
                
        elif "Just the Two of Us: 2000" in a:
            xpp = achiv.get("criteria")["is_completed"]
            if xpp is True:
                xpv2.append(2000)
       
                
        else:
            xpv2.append(0)
            
            

    # xp en v3
    for achiv in xp.json().get("achievements"):
        a = achiv.get("achievement")["name"]

    
        if "Three's Company: 2400" in a:
            xpp = achiv.get("criteria")["is_completed"]
            if xpp is True:
                xpv3.append(2400)
                
                
        elif "Three's Company: 2200" in a:
            xpp = achiv.get("criteria")["is_completed"]
            if xpp is True:
                xpv3.append(2200)
                
                
        elif "Three's Company: 2000" in a:
            xpp = achiv.get("criteria")["is_completed"]
            if xpp is True:
                xpv3.append(2000)
                
        
        elif "Three's Company: 1750" in a:
            xpp = achiv.get("criteria")["is_completed"]
            if xpp is True:
                xpv3.append(1750)


        elif "Gladiator" in a:
            xpv3.append(3000)
                
        else:
            xpv3.append(0)
            

    #xp rbg
    for achiv in xp.json().get("achievements"):
        a = achiv.get("achievement")["name"]
        if "High Warlord" == a:
            xprbg.append(2400)
            

        elif "Grand Marshal" == a:
            xprbg.append(2400)


        elif "Hero of the Horde" == a:
            xprbg.append(3000)


        elif "Hero of the Alliance" == a:
            xprbg.append(3000)
                    

        elif "Warlord" == a:
            xprbg.append(2300)


        elif "Field Marshal" == a:
            xprbg.append(2300)


        elif "General" == a:
            xprbg.append(2200)
                  
        elif "Marshal" == a:
            xprbg.append(2200)
            

        elif "Commander" == a:
            if achiv.get("achievement")["id"] == 5340:
                xprbg.append(2100)

        elif "Lieutenant General" == a:
            xprbg.append(2100)
            
        elif "Champion" == a:
            xprbg.append(2000)

        elif "Lieutenant Commander" == a:
            xprbg.append(2000)
            
        else:
            xprbg.append(0)
            

    xpv2.sort()
    xpv3.sort()
    xprbg.sort()
    return(xpv2[-1], xpv3[-1], xprbg[-1])
    

if __name__ == "__main__":
    # get_works()
    # get_realm()
    #get_pvp_arena("prevà", "tichondrius", get_token())
    #get_pvp_arena("sètth", "archimonde", get_token())
    get_xp("dudupie", "twisting-nether", get_token())
    #get_xp("sètth", "archimonde", get_token())
